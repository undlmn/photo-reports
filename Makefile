APP_DEV=phrep-dev
APP_PROD=phrep-prod

build: stop
	docker-compose -p $(APP_PROD) build

run: stop
	docker-compose -p $(APP_PROD) up -d

stop:
	docker-compose -p $(APP_PROD) down

rmi:
	-docker rmi `docker images -q -a phrep-api-prod`
	-docker rmi `docker images -q -a phrep-client-prod`

dev: dev-stop dev-rmi
	docker-compose -f docker-compose-dev.yml -p $(APP_DEV) up

dev-stop:
	docker-compose -f docker-compose-dev.yml -p $(APP_DEV) down

dev-rmi:
	-docker rmi `docker images -q -a phrep-api-devenv`
	-docker rmi `docker images -q -a phrep-client-devenv`

prune-all:
	docker system prune -a
