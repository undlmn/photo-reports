import http from "http";
import mongoose from "mongoose";
import { ApolloServer } from "apollo-server";
import {
  ApolloServerPluginLandingPageDisabled,
  ApolloServerPluginLandingPageLocalDefault,
} from "apollo-server-core";
import { defineModels } from "./mongoose/models.js";
import { initDatabase } from "./init.js";
import { typeDefs, resolvers, context } from "./graphql/index.js";

const { MONGO_URI } = process.env;
if (!MONGO_URI) {
  throw new Error("The MONGO_URI environment variable is required");
}

const db = await mongoose.createConnection(MONGO_URI);
const models = await defineModels(db);
await initDatabase(models);

const apollo = new ApolloServer({
  typeDefs,
  resolvers,
  context: context({ models }),
  plugins: [
    process.env.NODE_ENV === "production"
      ? ApolloServerPluginLandingPageDisabled()
      : ApolloServerPluginLandingPageLocalDefault({ footer: false }),
  ],
});

apollo.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
