export default {
  Mutation: {
    async logout(parent, args, { models: { UserSession }, stopSession }) {
      await stopSession(UserSession);
      return true;
    },
  },
};
