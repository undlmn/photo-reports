export default {
  Mutation: {
    async login(
      parent,
      { username, password },
      { models: { User, UserSession }, startSession }
    ) {
      const user = await User.login(username, password);
      if (user) {
        await startSession(UserSession, { user });
      }
      return !!user;
    },
  },
};
