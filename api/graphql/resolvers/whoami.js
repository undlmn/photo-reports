export default {
  Query: {
    async whoami(parent, args, { models: { UserSession }, getSession }) {
      const session = await getSession(UserSession);
      if (session) {
        return session.user.toObject();
      }
    },
  },
};
