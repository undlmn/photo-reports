import { getFiles } from "../lib/files.js";

const resolversURL = new URL("resolvers/", import.meta.url);

const resolvers = {};
for await (const moduleURL of getFiles(resolversURL, {
  filter: (name) => /^[^~_].*\.js$/.test(name),
})) {
  const { default: itemResolvers } = await import(moduleURL);
  if (itemResolvers == null) {
    throw new SyntaxError(`There is no default export in ${moduleURL}`);
  }

  for (const [type, itemTypeResolvers] of Object.entries(itemResolvers)) {
    if (resolvers[type]) {
      const names = Object.keys(resolvers[type]);
      Object.keys(itemTypeResolvers).forEach((name) => {
        if (names.includes(name)) {
          throw new SyntaxError(`Duplicate resolver '${name}'`);
        }
      });
    }

    resolvers[type] = { ...resolvers[type], ...itemTypeResolvers };
  }
}

export default resolvers;
