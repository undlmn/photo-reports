import { gql } from "apollo-server";

export default gql`
  type User {
    id: ID!
    username: String!
    uniqName: String!
    email: String
    createdAt: String
    updatedAt: String
    activityAt: String
  }

  type Query {
    whoami: User
  }

  type Mutation {
    login(username: String!, password: String!): Boolean!
    logout: Boolean!
  }
`;
