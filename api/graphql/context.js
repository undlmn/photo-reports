function getSessionCookieKey(Model) {
  const cookieKey = Model.cookieKey;
  if (cookieKey == null) {
    throw new SyntaxError(
      `There is no "cookieKey" static prop in ${Model.modelName}`
    );
  }
  return cookieKey;
}

function context({ models }) {
  return ({ req, res }) => {
    function setSessionCookie(key, value) {
      res.setHeader(
        "Set-Cookie",
        [
          `${key}=${value || ""}`,
          `Max-Age=${value ? 6e8 : 0}`, // ~ 19 years.
          "Path=/",
          "HttpOnly",
        ].join("; ")
      );
    }

    const sessions = new Map();

    async function getSession(Model) {
      let session = sessions.get(Model);
      if (!session) {
        const cookieKey = getSessionCookieKey(Model);
        // Get cookie
        const token = ((match) => match && match[1])(
          (req.headers.cookie || "").match(
            new RegExp(`(?:^|; )${cookieKey}=([^;]+)`)
          )
        );
        session = await Model.resume(token);
        if (token && !session) setSessionCookie(cookieKey, null);
        sessions.set(Model, session);
      }
      return session;
    }

    async function startSession(Model, data) {
      await stopSession(Model);
      const cookieKey = getSessionCookieKey(Model);
      const session = await Model.create(data, (token) =>
        setSessionCookie(cookieKey, token)
      );
      sessions.set(Model, session);
      return session;
    }

    async function stopSession(Model) {
      const session = await getSession(Model);
      if (session) {
        const cookieKey = getSessionCookieKey(Model);
        await session.remove();
        sessions.delete(Model);
        setSessionCookie(cookieKey, null);
      }
    }

    return { models, getSession, startSession, stopSession };
  };
}

export default context;
