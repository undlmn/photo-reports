import { basename } from "path";
import { getFiles } from "../lib/files.js";
import { toPascalCase, toKebabCase } from "../lib/naming.js";

const schemasURL = new URL("schemas/", import.meta.url);

export async function defineModels(connection) {
  const models = Object.create(null);
  for await (const moduleURL of getFiles(schemasURL, {
    filter: (name) => /^[^~_].*\.js$/.test(name),
  })) {
    const moduleBasename = basename(moduleURL.pathname, ".js");
    const modelName = toPascalCase(moduleBasename);
    const collectionName = toKebabCase(moduleBasename) + "s";

    const { default: schema, collectionName: redefinedCollectionName } =
      await import(moduleURL);
    if (schema == null) {
      throw new SyntaxError(`There is no default export in ${moduleURL}`);
    }

    models[modelName] = connection.model(
      modelName,
      schema,
      redefinedCollectionName || collectionName
    );
  }
  return models;
}
