import mongoose from "mongoose";
import bcrypt from "bcrypt";
import Bin from "../../lib/binary-data-array.js";
import { randomBytes } from "../../lib/utils.js";

export const collectionName = "users-sessions";

const userSessionSchema = new mongoose.Schema(
  {
    keyHash: {
      type: String,
      required: [true, "REQUIRED"],
      unique: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: [true, "REQUIRED"],
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    touchedAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toObject: {
      transform(doc, ret) {
        ret.id = doc.id;
        delete ret._id;
        delete ret.keyHash;
        return ret;
      },
    },
  }
);

userSessionSchema.loadClass(
  class {
    static cookieKey = "u";

    async createToken() {
      const key = Bin.from(await randomBytes(12)).toString("base32a"); // 20 chars
      this.keyHash = await bcrypt.hash(key, 10);
      return key + Bin.from(this.id, "hex").toString("base32a"); // 20+20=40 chars
    }

    static async create({ user }, onToken) {
      const session = new this({ user });
      onToken(await session.createToken());
      await session.save();

      user.activityAt = Date.now();
      await user.save({ timestamps: false });

      return session;
    }

    static async resume(token) {
      if (typeof token != "string" || token.length != 40) return null;

      const id = Bin.from(token.slice(-20), "base32a").toString("hex");
      const key = token.slice(0, 20);
      const session = await this.findById(id).populate("user").exec();
      if (session == null) return null;

      if (!(await bcrypt.compare(key, session.keyHash))) return null;

      session.touchedAt = session.user.activityAt = Date.now();
      await session.save();
      await session.user.save({ timestamps: false });

      return session;
    }
  }
);

export default userSessionSchema;
