import mongoose from "mongoose";
import bcrypt from "bcrypt";

function isValidUsername(value) {
  return /^[a-z0-9]+(?:[._-][a-z0-9]+)*$/i.test(value); // Xx.x_x-x
}

function isValidEmail(value) {
  return /^[^\s@]+@[^\s@.]+(?:\.[^\s@.]+)*\.[^\s@.]{2,}$/.test(value); // x@x.xx
}

function isValidPassword(value) {
  return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/.test(value); // Xx*****8
}

function uniqName(username) {
  return username.toLowerCase().replace(/[._]+/g, "-");
}

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      trim: true,
      required: [true, "REQUIRED"],
      validate: [isValidUsername, "INVALID"],
    },

    uniqName: {
      type: String,
      required: [true, "REQUIRED"],
      unique: true,
    },

    email: {
      type: String,
      trim: true,
      lowercase: true,
      validate: [isValidEmail, "INVALID"],
      index: { unique: true, sparse: true }, // unique, unless it isn't defined
    },

    passHash: {
      type: String,
      required: [true, "REQUIRED"],
    },

    activityAt: {
      type: Date,
    },
  },
  {
    timestamps: true, // createdAt, updatedAt

    toObject: {
      transform(doc, ret) {
        ret.id = doc.id;
        delete ret._id;
        delete ret.password;
        delete ret.passHash;
        return ret;
      },
    },
  }
);

userSchema
  .virtual("password")
  .get(function () {
    return this._password;
  })
  .set(function (value) {
    this._password = value;
  });

userSchema.pre("validate", async function () {
  if (typeof this.username == "string") {
    this.uniqName = uniqName(this.username);
  }

  if (typeof this.password == "string") {
    if (isValidPassword(this.password)) {
      this.passHash = await bcrypt.hash(this.password, 10);
    } else {
      this.invalidate("password", "INVALID");
    }
  } else if (this.isNew && this.passHash == null) {
    this.invalidate("password", "REQUIRED");
  }
});

userSchema.loadClass(
  class {
    async checkPassword(password) {
      return (
        typeof password == "string" &&
        (await bcrypt.compare(password, this.passHash))
      );
    }

    static async login(username, password) {
      if (typeof username != "string") return null;

      const user = isValidEmail(username)
        ? await this.findOne({ email: username.toLowerCase() })
        : isValidUsername(username)
        ? await this.findOne({ uniqName: uniqName(username) })
        : null;

      return user == null || !(await user.checkPassword(password))
        ? null
        : user;
    }
  }
);

export default userSchema;
