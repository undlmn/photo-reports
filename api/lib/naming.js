// as TwoWords
export function toPascalCase(str) {
  return str
    .split(/[\W_]+/)
    .filter(Boolean)
    .map((word) => word.replace(/\w/, (char) => char.toUpperCase()))
    .join("");
}

// as twoWords
export function toCamelCase(str) {
  return toPascalCase(str).replace(/\w/, (char) => char.toLowerCase());
}

// as two-words
export function toKebabCase(str) {
  return str
    .split(/[\W_]+/)
    .filter(Boolean)
    .map((word) => word.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase())
    .join("-");
}

// as two_words
export function toSnakeCase(str) {
  return toKebabCase(str).replace(/-/g, "_");
}
