import { readdir } from "fs/promises";

export async function* getFiles(url, options = {}) {
  const { filter = () => true, recursively = true } = options;

  for (const item of await readdir(url, { withFileTypes: true })) {
    if (!filter(item.name, url)) continue;

    if (recursively && item.isDirectory()) {
      yield* getFiles(new URL(item.name + "/", url), options);
    } else if (item.isFile()) {
      yield new URL(item.name, url);
    }
  }
}
