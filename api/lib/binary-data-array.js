import { Encoder, Decoder } from "./base-encode.js";

const bases = {
  base16: "0123456789abcdef",
  base16a: "acefghkmnrstwxyz",
  base32: "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567",
  base32a: "0123456789abcdefghjkmnpqrtuvwxyz",
  base64: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
  base64url: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_",
};

class Bin extends Uint8Array {
  static from(...args) {
    if (typeof args[0] != "string") return super.from(...args);

    const [str, enc] = args;

    if (enc == "hex") {
      const len = Math.ceil(str.length / 2);
      const res = new Bin(len);
      for (let i = 0; i < len; i++) {
        res[i] = parseInt(str.substr(2 * i, 2), 16);
      }
      return res;
    }

    if (enc && bases[enc]) {
      const decoder = Decoder(bases[enc]);
      return Bin.concat(decoder.data(str), decoder.end());
    }

    return super.from(new TextEncoder().encode(str));
  }

  static concat(...arrs) {
    const res = new Bin(arrs.reduce((len, arr) => len + arr.length, 0));
    let pos = 0;
    for (const arr of arrs) {
      res.set(arr, pos);
      pos += arr.length;
    }
    return res;
  }

  static xor(a, b, target) {
    const len = a.length;
    if (b.length !== len || (target && target.length !== len))
      throw new TypeError();
    const res = target || new Bin(len);
    for (let i = 0; i < len; i++) {
      res[i] = a[i] ^ b[i];
    }
    return res;
  }

  toString(enc) {
    if (enc == "decimal") return super.toString();

    if (enc == "hex") {
      const h = "0123456789abcdef";
      let str = "";
      for (const byte of this) {
        str += h.charAt((byte & 0xf0) >> 4) + h.charAt(byte & 0x0f);
      }
      return str;
    }

    if (enc && bases[enc]) {
      const encoder = Encoder(bases[enc]);
      return Bin.concat(encoder.data(this), encoder.end()).toString();
    }

    return new TextDecoder(enc).decode(this);
  }

  concat(...arrs) {
    return Bin.concat(this, ...arrs);
  }

  xor(arr) {
    return Bin.xor(this, arr, this);
  }
}

export default Bin;
