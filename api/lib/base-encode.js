const stringToArray = (str) =>
  typeof str == "string" ? new TextEncoder().encode(str) : str;

export function Encoder(alphabet) {
  alphabet = stringToArray(alphabet);
  const len = alphabet.length;
  if (typeof len != "number" || len < 2) throw new TypeError();

  const bits = Math.min(8, Math.log2(len) | 0);
  let skip = 0;
  let read = 0;

  return {
    data(chunk) {
      chunk = stringToArray(chunk);
      const output = [];
      for (const byte of chunk) {
        while (true) {
          if (skip < 0) {
            // we have a carry from the previous byte
            read |= byte >>> -skip;
          } else {
            // no carry
            read = (byte << skip) & 0xff;
          }
          if (skip <= 8 - bits) {
            output.push(alphabet[read >>> (8 - bits)]);
            skip += bits;
          } else {
            // not enough data
            break;
          }
        }
        skip -= 8;
      }
      return Uint8Array.from(output);
    },
    end() {
      const output = skip < 0 ? [alphabet[read >>> (8 - bits)]] : [];
      skip = 0;
      read = 0;
      return Uint8Array.from(output);
    },
  };
}

export function Decoder(alphabet) {
  alphabet = stringToArray(alphabet);
  const len = alphabet.length;
  if (typeof len != "number" || len < 2) throw new TypeError();

  const bits = Math.min(8, Math.log2(len) | 0);
  const lookup = {};
  for (let i = 0; i < len; i++) {
    lookup[alphabet[i]] = i;
  }
  let skip = 0;
  let read = 0;

  return {
    data(chunk) {
      chunk = stringToArray(chunk);
      const output = [];
      for (const byte of chunk) {
        let value = lookup[byte];
        if (value == null) continue;
        value <<= 8 - bits; // move to the high bits
        read |= value >>> skip;
        skip += bits;
        if (skip >= 8) {
          // we have enough to preduce output
          output.push(read);
          skip -= 8;
          if (skip > 0) {
            read = (value << (bits - skip)) & 0xff;
          } else {
            read = 0;
          }
        }
      }
      return Uint8Array.from(output);
    },
    end() {
      skip = 0;
      read = 0;
      return new Uint8Array();
    },
  };
}
