import crypto from "crypto";

export function randomBytes(size, encoding) {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(size, (err, buf) => {
      err ? reject(err) : resolve(encoding ? buf.toString(encoding) : buf);
    });
  });
}
