export async function initDatabase(models) {
  for (const Model of Object.values(models)) {
    await Model.syncIndexes();
    await Model.init();
  }

  if (!(await models.User.count({}))) {
    console.log("🤔  Users collection is empty");
    console.log('Creating an initial user { username: "a", password "a" }');
    const user = new models.User({
      username: "a",
      passHash: "$2b$10$irGN8aBTyGAwx0TZpyrWSu0ACyVotEQ0D0n9OuY05AS.qt1hvqkdK",
    });
    await user.save();
    console.log("Done");
  }
}
