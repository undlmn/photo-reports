export function listen(node, event, handler, options) {
  node.addEventListener(event, handler, options);
  return () => {
    node.removeEventListener(event, handler, options);
  };
}

export function attr(node, attribute, newValue) {
  const oldValue = node.getAttribute(attribute);
  if (typeof newValue == "function") newValue = newValue(oldValue);

  // null/false -> remove attribute
  // undefined  -> get current value only
  // other      -> set attribute
  if (newValue === null || newValue === false) {
    node.removeAttribute(attribute);
  } else if (newValue != null) {
    newValue = newValue === true ? "" : "" + newValue;
    newValue !== oldValue && node.setAttribute(attribute, newValue);
  }
  // Always returns old value
  return oldValue;
}

export function isEnclosed(node, ...tagNames) {
  while (node) {
    if (tagNames.includes((node.tagName || "").toLowerCase())) return true;
    node = node.parentElement;
  }
  return false;
}
