export const ssr = typeof window == "undefined";

export const dev = process.env.NODE_ENV === "development";
