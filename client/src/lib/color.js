function parseColor(hex) {
  if (hex[0] === "#") hex = hex.slice(1);
  const n = parseInt(hex.length === 3 ? hex.replace(/(\w)/g, "$1$1") : hex, 16);
  return [(n >> 16) & 255, (n >> 8) & 255, n & 255]; // [red, green, blue]
}

export function mix(color1, color2, weight = 0.5) {
  // weight: number between 0 and 1
  // 0 -> color2
  // 1 -> color1
  const c1 = parseColor(color1);
  const c2 = parseColor(color2);
  return c2.reduce(
    (h, c, i) =>
      h +
      ((c + Math.round((c1[i] - c) * weight)) & 255)
        .toString(16)
        .padStart(2, "0"),
    "#"
  );
}

export function rgba(color, alpha = 1) {
  return `rgba(${parseColor(color).join(", ")}, ${alpha})`;
}
