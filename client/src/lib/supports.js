import { ssr } from "./env.js";

export const supportsFocusVisible =
  ssr ||
  (() => {
    try {
      return !!CSS.supports("selector(:focus-visible)");
    } catch {}
    return false;
  })();
