const q0000 = 43670016;

// Number to string as [a-z][0-9a-z]{4} (a0000-zzzzz)
export function a0000(num) {
  num = Math.floor(num) % q0000;
  if (isNaN(num)) num = 0;
  if (num < 0) num += q0000;
  return (q0000 / 2.6 + num).toString(36);
}

export function genKey() {
  return a0000(Math.random() * q0000);
}

export function calcHash(str) {
  let num = 5381;
  for (let i = str.length; i--; ) {
    num = (num * 33) ^ str.charCodeAt(i);
  }
  return a0000(num);
}

export function uniqueKeysPool() {
  const keys = new Set();
  return function uniqueKey() {
    const key = genKey();
    if (keys.has(key)) {
      return uniqueKey();
    }
    keys.add(key);
    return key;
  };
}

export function uniqueHashesPool() {
  const hashes = new Map();
  return function uniqueHash(str) {
    const hash = calcHash(str);
    if (hashes.has(hash)) {
      if (hashes.get(hash) !== str) {
        return uniqueHash(str + ".");
      }
    } else {
      hashes.set(hash, str);
    }
    return hash;
  };
}
