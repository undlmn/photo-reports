import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { tw, apply } from "twind";
import Spinner from "icons/spinner.jsx";
import { supportsFocusVisible } from "lib/supports.js";

const variantMap = {
  primary: ["sky", 650],
  secondary: ["gray", 500],
  success: ["emerald", 650],
  danger: ["red", 550],
  warning: ["amber", 400],
  info: ["cyan", 450],
};

const baseStyles = apply`
  items-center justify-center
  border overflow-hidden whitespace-nowrap
  sm:text-sm select-none
  focus:outline-none
  ${
    supportsFocusVisible && "focus-visible:(ring(2 offset(gray(100 dark:800))))"
  }
`;

Button.propTypes = {
  inline: PropTypes.bool,
  disabled: PropTypes.bool,
  active: PropTypes.bool,
  variant: PropTypes.oneOf(Object.keys(variantMap)),
  small: PropTypes.bool,
  tight: PropTypes.bool,
  rounded: PropTypes.bool,
  outlined: PropTypes.bool,
  minimal: PropTypes.bool,
  loading: PropTypes.bool,
  loadingLabel: PropTypes.string,
};

function Button({
  inline = false,
  active = false,
  variant = "secondary",
  small = false,
  tight = false,
  rounded = false,
  outlined = false,
  minimal = false,
  loading = false,
  loadingLabel = "Загрузка...",
  tw: customStyle,
  children,
  ...props
}) {
  if (loading) {
    props.disabled = true;
  }
  if (minimal) {
    outlined = false;
  }

  const Component = props.to ? Link : props.href ? "a" : "button";

  const [colorName, level] = variantMap[variant];
  const isLight = level < 500;
  const color = `${colorName}-${level}`;
  const hoverColor = `${colorName}-${level + 50}`;
  const lineColor = `${colorName}-${isLight ? level + 100 : level}`;
  const lineColorOnDark = `${colorName}-${isLight ? level : level - 150}`;

  const instanceStyles = apply(
    inline ? "inline-flex" : "flex",

    props.disabled && "opacity-50 pointer-events-none",

    outlined || minimal
      ? `text(${lineColor} dark:${lineColorOnDark})
        font(semibold dark:medium) bg-${lineColor} ${
          active
            ? "bg-opacity-25"
            : "bg-opacity(0 hover:not-active:10 active:25)"
        }`
      : `border-transparent text-${isLight ? "black" : "white"}
        font-medium shadow-sm ${
          active
            ? `bg-shade-20-${hoverColor}`
            : `bg(${color} hover:not-active:${hoverColor})
              active:bg-shade-20-${hoverColor}`
        }`,

    !tight && (small ? "py-1 px-2.5" : "py-2 px-3"),

    `rounded-${rounded ? "full" : "md"}`,

    outlined && `border(${lineColor} dark:${lineColorOnDark})`,

    minimal && "border-transparent",

    supportsFocusVisible &&
      `focus-visible:ring(offset-${minimal ? 0 : 2} ${color})`
  );

  props.className = tw(baseStyles, instanceStyles, customStyle);

  return (
    <Component {...props}>
      {loading ? (
        <>
          <Spinner tw="mr-2 stroke-2.5" />
          {loadingLabel}
        </>
      ) : (
        children
      )}
    </Component>
  );
}

export default Button;
