import PropTypes from "prop-types";
import { tw, apply } from "twind";

const inputBaseStyles = apply`
  appearance-none block w-full rounded-md
  text-gray(900 dark:300) placeholder-gray(400 dark:500)
  sm:text-sm dark:font-light
  focus:outline-none
`;

export const labelStyle = apply`
  text(sm gray(700 dark:350)) font(medium dark:light) select-none
`;

export const descriptionStyle = apply`
  text(sm gray(500 dark:450))
`;

export const errorMessageStyle = apply`
  text(sm red(500 dark:450))
`;

export const disabledStyle = apply`
  opacity(60 dark:40)
`;

Input.propTypes = {
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  label: PropTypes.string,
  description: PropTypes.string,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  as: PropTypes.oneOf(["input", "select", "textarea"]),
};

function Input({
  label,
  description,
  error,
  as: Element = "input",
  tw: customStyle,
  ...props
}) {
  if (Element === "input" && props.type == null) {
    props.type = "text";
  }

  const isSingle =
    label == null &&
    description == null &&
    (error == null || typeof error == "boolean");

  const instanceStyles = apply(
    error
      ? `border(& red(500 dark:450))
        focus:(border-red(500 dark:450) ring-red(500 dark:450))`
      : `border(& gray-450 opacity-40)
        focus:(border-sky(500 dark:600) ring-sky(500 dark:600))`,

    props.readOnly ? "bg-transparent" : "bg(white dark:(black opacity-20))"
  );

  props.className = tw(
    inputBaseStyles,
    instanceStyles,
    props.disabled && "pointer-events-none",
    isSingle && props.disabled && disabledStyle,
    isSingle && customStyle
  );

  return isSingle ? (
    <Element {...props} />
  ) : (
    <div className={tw(props.disabled && disabledStyle, customStyle)}>
      {label && (
        <label className={tw("block mb-1", labelStyle)} htmlFor={props.id}>
          {label}
        </label>
      )}
      <Element {...props} />
      {error && error !== true && (
        <p className={tw("mt-1", errorMessageStyle)}>{error}</p>
      )}
      {description && (
        <p className={tw("mt-1", descriptionStyle)}>{description}</p>
      )}
    </div>
  );
}

export default Input;
