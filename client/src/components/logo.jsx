import PropTypes from "prop-types";
import { tw } from "twind";

Logo.propTypes = {
  size: PropTypes.string,
  mono: PropTypes.bool,
  currentColor: PropTypes.bool,
};

function Logo({
  size = "14",
  mono = false,
  currentColor = false,
  tw: customStyle,
}) {
  return (
    <svg
      viewBox="0 0 24 24"
      className={tw`w-${size} h-${size} flex-none ${customStyle}`}
    >
      <path
        fill="currentColor"
        className={
          !mono ? tw`text-cyan-500` : currentColor ? null : tw`text-gray-500`
        }
        d="M20.1 5.32l-2 2a.6.6 95 00-.06.76 7.2 7.2 0 011.14 3.42 7.23 7.23 90 010 1 7.2 7.2 0 01-1.14 3.42.6.6 85 00.06.77l2 2a.44.44 176.74 00.66-.04A11 11 0 0023 12.5a10.97 10.97 90 000-1 11 11 0 00-2.23-6.15.44.44 3.26 00-.66-.03zM3.9 18.68l2-2a.6.6 0 00.06-.76 7.2 7.2 0 01-1.14-3.42 7.23 7.23 0 010-1 7.2 7.2 0 011.14-3.42.6.6 0 00-.06-.77l-2-2a.44.44 0 00-.66.04A11 11 0 001 11.5a10.97 10.97 0 000 1 11 11 0 002.23 6.15.44.44 0 00.66.03z"
      />
      <path
        fill="currentColor"
        className={
          currentColor
            ? null
            : mono
            ? tw`text-gray-500`
            : tw`text(sky-700 dark:gray-350)`
        }
        d="M11.97 7.6A4.4 4.4 0 007.6 12a4.4 4.4 0 004.4 4.4 4.4 4.4 0 004.4-4.4A4.4 4.4 0 0012 7.6a4.4 4.4 0 00-.03 0zM13.19 9A1.8 1.8 0 0115 10.8a1.8 1.8 0 01-1.8 1.8 1.8 1.8 0 01-1.8-1.8A1.8 1.8 0 0113.2 9zM11.5 1.01a11 11 0 00-6.15 2.23.44.44 0 00-.03.66l2 2a.6.6 0 00.76.06 7.2 7.2 0 013.42-1.14 7.23 7.23 0 011 0 7.2 7.2 0 013.42 1.14.6.6 0 00.77-.06l2-2a.44.44 0 00-.04-.66A11 11 0 0012.5 1a10.97 10.97 0 00-1 0zm7.18 19.09l-2-2a.6.6 0 00-.76-.06 7.2 7.2 0 01-3.42 1.14 7.23 7.23 0 01-1 0 7.2 7.2 0 01-3.42-1.14.6.6 0 00-.77.06l-2 2a.44.44 0 00.04.66A11 11 0 0011.5 23a10.97 10.97 0 001 0 11 11 0 006.15-2.23.44.44 0 00.03-.66z"
      />
    </svg>
  );
}

export default Logo;
