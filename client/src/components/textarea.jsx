import Input from "./input.jsx";

function Textarea(props) {
  return <Input as="textarea" {...props} />;
}

export default Textarea;
