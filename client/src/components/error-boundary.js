import { Component } from "react";

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  static getDerivedStateFromError(error) {
    return { error };
  }

  render() {
    const { fallback, children } = this.props;
    return this.state.error ? fallback : children;
  }
}

export default ErrorBoundary;
