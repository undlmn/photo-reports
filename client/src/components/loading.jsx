import PropTypes from "prop-types";
import { tw, animation } from "twind/css";
import IconSpinner from "icons/spinner.jsx";

const fadeInStyle = animation("150ms linear", {
  from: {
    opacity: 0,
  },
  to: {
    opacity: 1,
  },
});

Loading.propTypes = {
  label: PropTypes.string,
};

function Loading({ label = "Загрузка...", tw: customStyle }) {
  return (
    <div className={tw("flex items-center", fadeInStyle, customStyle)}>
      <IconSpinner tw={label && "mr-2"} />
      {label}
    </div>
  );
}

export default Loading;
