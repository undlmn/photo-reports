import Input from "./input.jsx";

function Select(props) {
  return <Input as="select" {...props} />;
}

export default Select;
