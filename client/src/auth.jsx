import { createContext, useContext } from "react";
import { gql, useQuery } from "@apollo/client";

const WHOAMI = gql`
  query Whoami {
    whoami {
      id
      username
      uniqName
    }
  }
`;

const AuthContext = createContext([]);

export function AuthProvider({ children }) {
  const { error, loading, data } = useQuery(WHOAMI);

  if (error) throw new Error(error);

  return (
    <AuthContext.Provider value={[loading, data?.whoami]}>
      {children}
    </AuthContext.Provider>
  );
}

export const useAuth = () => useContext(AuthContext);
