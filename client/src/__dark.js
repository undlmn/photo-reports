// Copy of inline script from public/index.html - Do not edit
//
// 1. Reflect dark color scheme from localStorage or matchMedia
//    as <html class="dark">
// 2. Append window.colorScheme function
//    param: (string: 'dark' / any) | null - use prefers
// 3. Dispatch event 'colorScheme' on update

((
  w, // window
  l, // localStorage
  c, // colorScheme
  d, // dark
  t, // matchMedia
  a, // addEventListener
  b, // addListener
  m = w[t] ? w[t](`(prefers-color-scheme: dark)`) : {},
  r = (s) => {
    document.documentElement.classList[
      ((s = l.getItem(c)) == null ? m.matches : s == d) ? "add" : "remove"
    ](d);
    dispatchEvent(new Event(c));
  }
) =>
  r(
    // Listen matchMedia
    m[a] ? m[a]("change", r) : m[b] && m[b](r),
    // Listen localStorage
    w[a]("storage", (e) => (e.key == c || e.key == null) && r()),
    // Append function colorScheme
    (w[c] = (s) => r(l[s ? "setItem" : "removeItem"](c, s)))
  ))(
  window,
  localStorage,
  "colorScheme",
  "dark",
  "matchMedia",
  "addEventListener",
  "addListener"
);
