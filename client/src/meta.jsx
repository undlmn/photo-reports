import { createContext, useContext, useEffect } from "react";
import { attr } from "lib/utils.js";

const TITLE_ADDON = " | Фотоотчеты";

const originalValueSymbol = Symbol();

const MetaContext = createContext(null);

// SSR only
export function MetaProvider({ value, children }) {
  return <MetaContext.Provider value={value}>{children}</MetaContext.Provider>;
}

function text(node, newValue) {
  const oldValue = node.textContent;
  if (newValue !== undefined && newValue !== oldValue) {
    node.textContent = newValue;
  }
  return oldValue;
}

const attrContent = (node, newValue) => attr(node, "content", newValue);

export function useMeta(key, value) {
  const meta = useContext(MetaContext);
  if (meta) {
    meta[key] = value;
  }

  useEffect(() => {
    if (key === "status") return;

    const isTitle = key === "title";
    const element = isTitle ? "title" : "meta";
    const accessor = isTitle ? text : attrContent;
    let node = document.head.querySelector(
      `${element}${isTitle ? "" : `[name="${key}"]`}`
    );
    if (!node) {
      node = document.createElement(element);
      isTitle || attr(node, "name", key);
      document.head.appendChild(node);
    }

    const oldValue = accessor(node, value);
    if (node[originalValueSymbol] == null) {
      node[originalValueSymbol] = oldValue;
    }

    return () => {
      accessor(node, node[originalValueSymbol]);
    };
  }, [key, value]);
}

export const useTitle = (value) => useMeta("title", value + TITLE_ADDON);
export const useDescription = (value) => useMeta("description", value);
export const useKeywords = (value) => useMeta("keywords", value);
export const useStatus = (value) => useMeta("status", value); // SSR only
