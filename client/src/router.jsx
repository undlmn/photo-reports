import { createContext, useContext, useMemo } from "react";
import { useLocation, useNavigate, matchPath } from "react-router-dom";
import LayoutBlank from "layouts/blank.jsx";
import PageNotFound from "pages/not-found.jsx";
import PageUnauthorized from "pages/unauthorized.jsx";
import Loading from "components/loading.jsx";
import { useAuth } from "auth.jsx";
import routes from "routes.js";

const RouteParamsContext = createContext({});

export const useRouteParams = () => useContext(RouteParamsContext);

function Router() {
  const location = useLocation();
  const navigate = useNavigate();
  const [loading, user] = useAuth();

  const [Layout = LayoutBlank, layoutProps = {}, routeParams = {}] =
    useMemo(() => {
      let authenticationRequired = false;
      let unauthorized = false;
      for (const {
        path,
        end = true,
        auth,
        stop = false,
        layout,
        ...props
      } of routes) {
        const match = matchPath({ path, end }, location.pathname);
        if (match) {
          if (auth) {
            if (loading) {
              return [LayoutBlank, { content: Loading }];
            }
            if (!user) {
              authenticationRequired = true;
              if (stop) {
                break;
              } else {
                continue;
              }
            }
            if (typeof auth == "function" && !auth(user)) {
              unauthorized = true;
              if (stop) {
                break;
              } else {
                continue;
              }
            }
          }
          return [layout, props, match.params];
        }
      }
      if (authenticationRequired) {
        setTimeout(() =>
          navigate("/login", { replace: true, state: { from: location } })
        );
        return [LayoutBlank, { content: Loading }];
      }
      if (unauthorized) {
        return [LayoutBlank, { content: PageUnauthorized, from: location }];
      }
      return [LayoutBlank, { content: PageNotFound }];
    }, [location, navigate, loading, user]);

  return (
    <RouteParamsContext.Provider value={routeParams}>
      <Layout {...layoutProps} />
    </RouteParamsContext.Provider>
  );
}

export default Router;
