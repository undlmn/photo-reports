import { Link } from "react-router-dom";
import { tw } from "twind";
import IconArrowLeft from "icons/arrow-left.jsx";
import { useStatus, useTitle } from "meta.jsx";

function Unauthorized({ from }) {
  useStatus(401);
  useTitle("Не авторизованый доступ");

  return (
    <div className={tw`w-full max-w-xs p-6`}>
      <h1 className={tw`text-2xl`}>Доступ запрещен</h1>
      <p className={tw`mt-2`}>Нет разрешения на доступ к этой странице</p>
      <p className={tw`mt-6`}>
        <Link
          className={tw`underline`}
          to={{ pathname: "/login", state: { from } }}
        >
          Войти под другим пользователем
        </Link>
      </p>
      <p className={tw`mt-12`}>
        <Link className={tw`inline-flex items-center`} to="/">
          <IconArrowLeft tw="mr-2" />
          На главную страницу
        </Link>
      </p>
    </div>
  );
}

export default Unauthorized;
