import { tw } from "twind";
import IconArrowLeft from "icons/arrow-left.jsx";
import { useStatus, useTitle } from "meta.jsx";

function Error() {
  useStatus(500);
  useTitle("Ошибка");

  return (
    <div className={tw`w-full max-w-xs p-6`}>
      <h1 className={tw`text-2xl`}>Что-то пошло не так</h1>
      <p className={tw`mt-2`}>Попробуйте ещё раз</p>
      <p className={tw`mt-12`}>
        <a className={tw`inline-flex items-center`} href="/">
          <IconArrowLeft tw="mr-2" />
          На главную страницу
        </a>
      </p>
    </div>
  );
}

export default Error;
