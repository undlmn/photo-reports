import { useState, useCallback } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { gql, useMutation } from "@apollo/client";
import { tw } from "twind";
import Button from "components/button.jsx";
import IconLogOut from "icons/log-out.jsx";
import IconArrowLeft from "icons/arrow-left.jsx";
import Logo from "components/logo.jsx";
import { useTitle } from "meta.jsx";
import { commonErrorMessage } from "misc/messages.js";

const LOGOUT = gql`
  mutation Logout {
    logout
  }
`;

function Logout() {
  useTitle("Выход");

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const location = useLocation();

  const [logout, { client }] = useMutation(LOGOUT, {
    onError({ message }) {
      setLoading(false);
      setError(commonErrorMessage(message));
    },
    async onCompleted() {
      await client.resetStore();
      navigate("/", { replace: true });
    },
  });

  const handleSubmit = useCallback(() => {
    setLoading(true);
    logout();
  }, [logout]);

  return (
    <main className={tw`w-full max-w-xs p-4 space-y-12`}>
      <div className={tw`space-y-6`}>
        <div className={tw`space-y-8`}>
          <Link to="/">
            <Logo tw="mx-auto" mono />
          </Link>
          <h2 className={tw`text(center 2xl)`}>
            Вы уверены, что хотите выйти?
          </h2>
        </div>
        <div className={tw`space-y-4`}>
          {error && (
            <p
              className={tw`
              text(red(500 dark:450) center) font(medium dark:normal)
          `}
            >
              {error}
            </p>
          )}
          <Button tw="relative w-full" onClick={handleSubmit} loading={loading}>
            <IconLogOut
              tw="absolute left-2.5 top-1/2 -translate-y-1/2 opacity-25"
              size="5"
            />
            Выйти
          </Button>
        </div>
      </div>
      {location.state?.from && (
        <p className={tw`text-center`}>
          <Link
            className={tw`inline-flex items-center -translate-x-2`}
            to={location.state.from}
          >
            <IconArrowLeft tw="mr-2" />
            Вернуться обратно
          </Link>
        </p>
      )}
    </main>
  );
}

export default Logout;
