import { useState, useCallback } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { gql, useMutation } from "@apollo/client";
import { tw, animation } from "twind/css";
import Button from "components/button.jsx";
import Input from "components/input.jsx";
import IconUnlock from "icons/unlock.jsx";
import Logo from "components/logo.jsx";
import { useTitle } from "meta.jsx";
import { useFormData, useFormSubmit } from "hooks/form.js";
import { useTimer } from "hooks/timer.js";
import { commonErrorMessage } from "misc/messages.js";

const LOGIN = gql`
  mutation Login($username: String!, $password: String!) {
    login(username: $username, password: $password)
  }
`;

const shakeStyle = animation("120ms linear 2", {
  "0%, 50%, 100%": {
    transform: "translateX(0)",
  },
  "25%": {
    transform: "translateX(-3%)",
  },
  "75%": {
    transform: "translateX(3%)",
  },
});

const LABEL_USERNAME = "Имя пользователя или email адрес";
const LABEL_PASSWORD = "Пароль";

function Login() {
  useTitle("Аутентификация");

  const [error, setError] = useState(null);
  const [errorHighlighting, setErrorHighlighting] = useState(false);
  const [loading, setLoading] = useState(false);
  const [shake, startShake] = useTimer(500);
  const navigate = useNavigate();
  const location = useLocation();

  const [formData, handleChange] = useFormData(
    {
      username: "",
      password: "",
    },
    useCallback(() => {
      setErrorHighlighting(false);
    }, [])
  );

  const [login, { client }] = useMutation(LOGIN, {
    variables: formData,
    onError({ message }) {
      setLoading(false);
      setError(commonErrorMessage(message));
    },
    async onCompleted({ login }) {
      if (login) {
        await client.resetStore();
        navigate(location.state?.from?.pathname || "/", { replace: true });
      } else {
        setLoading(false);
        setError("Неверное имя пользователя или пароль.");
        setErrorHighlighting(true);
        startShake();
      }
    },
  });

  const handleSubmit = useFormSubmit(
    useCallback(() => {
      setLoading(true);
      login();
    }, [login])
  );

  return (
    <main className={tw`w-full max-w-sm p-4 space-y-6`}>
      <div className={tw`space-y-8`}>
        <Link to="/">
          <Logo tw="mx-auto" mono />
        </Link>
        <h2 className={tw`text(center xl)`}>Войдите в свою учетную запись</h2>
      </div>
      <div className={tw`space-y-4`}>
        {error && (
          <p
            className={tw`
              text(red(500 dark:450) center) font(medium dark:normal)
            `}
          >
            {error}
          </p>
        )}
        <form method="POST" onSubmit={handleSubmit}>
          <div className={tw`space-y-6`}>
            <div className={tw("space-y-4", shake && shakeStyle)}>
              <Input
                aria-label={LABEL_USERNAME}
                placeholder={LABEL_USERNAME}
                name="username"
                autoComplete="username"
                value={formData.username}
                onChange={handleChange}
                error={errorHighlighting}
                required
              />
              <Input
                aria-label={LABEL_PASSWORD}
                placeholder={LABEL_PASSWORD}
                type="password"
                name="password"
                autoComplete="current-password"
                value={formData.password}
                onChange={handleChange}
                error={errorHighlighting}
                required
              />
            </div>
            <Button type="submit" tw="relative w-full" loading={loading}>
              <IconUnlock
                tw="absolute left-2.5 top-1/2 -translate-y-1/2 opacity-25"
                size="5"
              />
              Войти
            </Button>
          </div>
        </form>
      </div>
    </main>
  );
}

export default Login;
