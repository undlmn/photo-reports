import { Link } from "react-router-dom";
import { tw } from "twind";
import IconArrowLeft from "icons/arrow-left.jsx";
import { useStatus, useTitle } from "meta.jsx";

function NotFound() {
  useStatus(404);
  useTitle("Страница не найдена");

  return (
    <div className={tw`w-full max-w-xs p-6`}>
      <h1 className={tw`text-2xl`}>Страница не найдена</h1>
      <p className={tw`mt-2`}>
        Страница устарела, была удалена или не существовала вовсе
      </p>
      <p className={tw`mt-12`}>
        <Link className={tw`inline-flex items-center`} to="/">
          <IconArrowLeft tw="mr-2" />
          На главную страницу
        </Link>
      </p>
    </div>
  );
}

export default NotFound;
