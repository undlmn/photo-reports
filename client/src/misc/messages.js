export const commonErrorMessage = (message) =>
  message === "Failed to fetch"
    ? "Нет соединения. Проверьте подключение и попробуйте ещё раз."
    : "Что-то пошло не так. Попробуйте ещё раз позже.";
