import { StrictMode } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { setup as twindSetup } from "twind";
import { options as twindOptions } from "twind.js";
import App from "app.jsx";

twindSetup(twindOptions);

ReactDOM.render(
  <StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </StrictMode>,
  document.getElementById("root")
);
