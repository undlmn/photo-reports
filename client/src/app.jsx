import { Suspense } from "react";
import LayoutBlank from "layouts/blank.jsx";
import PageError from "pages/error.jsx";
import ErrorBoundary from "components/error-boundary.js";
import Loading from "components/loading.jsx";
import Apollo from "apollo.jsx";
import { AuthProvider } from "auth.jsx";
import Router from "router.jsx";

const error = <LayoutBlank content={PageError} />;
const loading = <LayoutBlank content={Loading} />;

function App() {
  return (
    <Apollo>
      <ErrorBoundary fallback={error}>
        <Suspense fallback={loading}>
          <AuthProvider>
            <Router />
          </AuthProvider>
        </Suspense>
      </ErrorBoundary>
    </Apollo>
  );
}

export default App;
