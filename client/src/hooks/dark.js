import { useState, useEffect, useCallback } from "react";
import { listen } from "lib/utils.js";
import { ssr } from "lib/env.js";

function hasClassDark() {
  return ssr ? false : document.documentElement.classList.contains("dark");
}

export function useDark() {
  const [isDark, setIsDark] = useState(hasClassDark());

  useEffect(
    () => listen(window, "colorScheme", () => setIsDark(hasClassDark())),
    []
  );

  const toggleDark = useCallback(() => {
    window.colorScheme(isDark ? "light" : "dark");
  }, [isDark]);

  return [isDark, toggleDark];
}
