import { useState, useRef, useCallback, useEffect } from "react";

const clearTimer = (timer) => clearTimeout(timer.current);
const setTimer = (timer, fn, time) => (timer.current = setTimeout(fn, time));

export function useTimer(
  defaultTime,
  onTimer /* important: use useCallback here! */
) {
  const [state, setState] = useState(false);
  const timer = useRef(null);

  const start = useCallback(
    (time = defaultTime) => {
      clearTimer(timer);
      setState(true);
      setTimer(
        timer,
        () => {
          setState(false);
          onTimer && onTimer();
        },
        time
      );
    },
    [defaultTime, onTimer]
  );

  const stop = useCallback(() => {
    clearTimer(timer);
    setState(false);
  }, []);

  useEffect(() => () => clearTimer(timer), []);

  return [state, start, stop];
}
