import { useState, useCallback } from "react";

export function useFormData(
  initialData = {},
  onChange /* important: use useCallback here! */
) {
  const [data, setData] = useState(initialData);

  const handleChange = useCallback(
    ({ target: { name, value } }) => {
      setData((data) => ({
        ...data,
        [name]: value,
      }));
      onChange && onChange(name, value);
    },
    [onChange]
  );

  return [data, handleChange, setData];
}

export function useFormSubmit(onSubmit /* important: use useCallback here! */) {
  return useCallback(
    (event) => {
      event.preventDefault();
      onSubmit && onSubmit();
    },
    [onSubmit]
  );
}
