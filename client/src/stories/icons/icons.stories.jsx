import { tw } from "twind";
import * as icons from "icons/index.js";

export default {
  title: "Icons/Icons",
  parameters: {
    controls: {
      hideNoControlsWarning: true,
    },
  },
};

export const All = () => (
  <div className={tw`flex flex-wrap`}>
    {Object.keys(icons).map((name) => {
      const Icon = icons[name];
      return (
        <div
          key={name}
          className={tw`
            flex flex-col items-center shadow m-1 p-2 w-32 h-28
            text-xs bg-gray(50 dark:750)
          `}
        >
          <div className={tw`flex-grow flex items-center`}>
            <Icon size="6" />
          </div>
          {name}
        </div>
      );
    })}
  </div>
);
