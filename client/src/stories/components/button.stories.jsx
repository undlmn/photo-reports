import { tw } from "twind";
import Button from "components/button.jsx";

export default {
  title: "Components/Button",
  component: Button,
  args: {
    inline: true,
    children: "Buttom",
  },
};

const variantsArgTypes = {
  variant: {
    table: {
      disable: true,
    },
  },
  children: {
    table: {
      disable: true,
    },
  },
};

const variants = "primary secondary success danger warning info".split` `;

const TemplateSingle = () => (args) => <Button {...args} />;

const TemplateVariants = () => (args) =>
  (
    <div className={tw`children:(mr-2 mb-2)`}>
      {variants.map((variant, i) => (
        <Button {...args} key={i} variant={variant}>
          {variant.replace(/^\w/, (c) => c.toUpperCase())}
        </Button>
      ))}
    </div>
  );

export const Default = TemplateSingle();

export const Variants = TemplateVariants();
Variants.argTypes = {
  ...variantsArgTypes,
};

export const Disabled = TemplateVariants();
Disabled.argTypes = {
  ...variantsArgTypes,
};
Disabled.args = {
  disabled: true,
};

export const Active = TemplateVariants();
Active.argTypes = {
  ...variantsArgTypes,
};
Active.args = {
  active: true,
};

export const Small = TemplateVariants();
Small.argTypes = {
  ...variantsArgTypes,
};
Small.args = {
  small: true,
};

export const Tight = TemplateVariants();
Tight.argTypes = {
  ...variantsArgTypes,
};
Tight.args = {
  tight: true,
};

export const Rounded = TemplateVariants();
Rounded.argTypes = {
  ...variantsArgTypes,
};
Rounded.args = {
  rounded: true,
};

export const Outlined = TemplateVariants();
Outlined.argTypes = {
  ...variantsArgTypes,
};
Outlined.args = {
  outlined: true,
};

export const Minimal = TemplateVariants();
Minimal.argTypes = {
  ...variantsArgTypes,
};
Minimal.args = {
  minimal: true,
};

export const Loading = TemplateVariants();
Loading.argTypes = {
  ...variantsArgTypes,
};
Loading.args = {
  loading: true,
};
