import Loading from "components/loading.jsx";

export default {
  title: "Components/Loading",
  component: Loading,
};

export const Default = (args) => <Loading {...args} />;
