import Input from "components/input.jsx";

export default {
  title: "Components/Input, Select, Textarea",
  component: Input,
};

const Template = () => (args) => <Input {...args} />;

export const Default = Template();

export const Label = Template();
Label.args = {
  id: "input-first-name",
  label: "First name",
};

export const Description = Template();
Description.args = {
  id: "input-password",
  label: "Password",
  type: "password",
  description:
    "Your password must be 8-20 characters long, contain letters and numbers.",
};

export const Disabled = Template();
Disabled.args = {
  id: "input-email",
  label: "Email address",
  type: "email",
  value: "email@example.com",
  description: "We'll never share your email with anyone else.",
  disabled: true,
};

export const ReadOnly = Template();
ReadOnly.args = {
  value: "email@example.com",
  readOnly: true,
};

export const Select = Template();
Select.argTypes = {
  children: {
    table: {
      disable: true,
    },
  },
};
Select.args = {
  as: "select",
  children: (
    <>
      <option>Open this select menu</option>
      <option value="1">One</option>
      <option value="2">Two</option>
      <option value="3">Three</option>
    </>
  ),
};

export const SelectMultiple = Template();
SelectMultiple.argTypes = {
  children: {
    table: {
      disable: true,
    },
  },
};
SelectMultiple.args = {
  as: "select",
  multiple: true,
  children: (
    <>
      <option value="1">One</option>
      <option value="2">Two</option>
      <option value="3">Three</option>
      <option value="4">Four</option>
    </>
  ),
};

export const Textarea = Template();
Textarea.args = {
  as: "textarea",
  placeholder: "Enter about...",
  rows: "4",
  id: "input-about",
  label: "About",
  description: "Brief description for your profile. URLs are hyperlinked.",
};

export const Error = Template();
Error.args = {
  error: true,
};

export const ErrorMessage = Template();
ErrorMessage.args = {
  placeholder: "Password",
  type: "password",
  error: "This field cannot be empty",
  description:
    "Your password must be 8-20 characters long, contain letters and numbers.",
};
