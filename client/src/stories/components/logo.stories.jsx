import Logo from "components/logo.jsx";

export default {
  title: "Components/Logo",
  component: Logo,
  argTypes: {
    size: {
      options: `
        4 5 6 7 8 9 10 11 12 14 16 20 24 28 32
        36 40 44 48 52 56 60 64 72 80 96 full`
        .split(/\s+/)
        .filter(Boolean),
      control: "select",
    },
  },
};

const Template = () => (args) => <Logo {...args} />;

export const Default = Template();

export const Small = Template();
Small.args = {
  size: "6",
};

export const CustomSize = Template();
CustomSize.argTypes = {
  size: {
    control: "text",
  },
};
CustomSize.args = {
  size: "[234px]",
};

export const Mono = Template();
Mono.args = {
  mono: true,
};

export const CurrentColor = Template();
CurrentColor.args = {
  currentColor: true,
};

export const MonoCurrentColor = Template();
MonoCurrentColor.storyName = "Mono and Current Color";
MonoCurrentColor.args = {
  mono: true,
  currentColor: true,
};
