import LayoutBlank from "layouts/blank.jsx";
import PageError from "pages/error.jsx";

export default {
  title: "Pages/Error",
  component: PageError,
  parameters: {
    controls: {
      hideNoControlsWarning: true,
    },
  },
};

export const Default = () => <LayoutBlank content={PageError} />;
