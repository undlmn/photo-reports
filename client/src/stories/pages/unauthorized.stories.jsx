import { BrowserRouter } from "react-router-dom";
import LayoutBlank from "layouts/blank.jsx";
import PageUnauthorized from "pages/unauthorized.jsx";

export default {
  title: "Pages/Unauthorized",
  component: PageUnauthorized,
  decorators: [
    (Story) => (
      <BrowserRouter>
        <Story />
      </BrowserRouter>
    ),
  ],
  parameters: {
    controls: {
      hideNoControlsWarning: true,
    },
  },
};

export const Default = () => <LayoutBlank content={PageUnauthorized} />;
