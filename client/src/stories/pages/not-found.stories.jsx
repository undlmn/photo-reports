import { BrowserRouter } from "react-router-dom";
import LayoutBlank from "layouts/blank.jsx";
import PageNotFound from "pages/not-found.jsx";

export default {
  title: "Pages/Not Found",
  component: PageNotFound,
  decorators: [
    (Story) => (
      <BrowserRouter>
        <Story />
      </BrowserRouter>
    ),
  ],
  parameters: {
    controls: {
      hideNoControlsWarning: true,
    },
  },
};

export const Default = () => <LayoutBlank content={PageNotFound} />;
