import { apply, strict, silent } from "twind";
import {
  gray,
  red,
  amber,
  yellow,
  green,
  emerald,
  cyan,
  lightBlue as sky,
  blue,
  indigo,
  purple,
  pink,
} from "twind/colors";
import { withForms } from "@twind/forms";
import { mix, rgba } from "lib/color.js";
import { uniqueHashesPool } from "lib/a0000.js";
import { dev } from "lib/env.js";

export const options = {
  mode: dev ? strict : silent,
  theme: {
    extend: {
      strokeWidth: { 1.5: "1.5", 2.5: "2.5", 3: "3" },
    },
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: "#000",
      white: "#fff",

      // Extend each color below with levels: 150, 250, ... 850
      ...Object.entries({
        gray,
        red,
        amber,
        yellow,
        green,
        emerald,
        cyan,
        sky,
        blue,
        indigo,
        purple,
        pink,
      }).reduce(
        (colors, [name, values]) => ({
          ...colors,
          [name]: {
            ...values,
            ...Object.fromEntries(
              [150, 250, 350, 450, 550, 650, 750, 850].map((level) => [
                level,
                mix(values[level - 50], values[level + 50]),
              ])
            ),
          },
        }),
        {}
      ),
    },
  },
  variants: {
    "not-first": "&:not(:first-child)",
    "not-last": "&:not(:last-child)",
  },
  plugins: {
    // Fix for twind v0.16.16
    // https://github.com/tw-in-js/twind/issues/185
    min: minMax,
    max: minMax,

    // Tint or shade a color: mix a color with white or black
    // bg-tint-20-blue-600
    "bg-tint": tintShade,
    "text-tint": tintShade,
    "bg-shade": tintShade,
    "text-shade": tintShade,
  },
  preflight: withForms({
    body: apply`bg-gray(100 dark:850) text-gray(800 dark:350) sm:text-sm dark:font-light`,
  }),
  darkMode: "class",
  hash: uniqueHashesPool(),
};

function minMax([k, ...rest], { theme }, id) {
  const key = { w: `${id}Width`, h: `${id}Height` }[k];
  if (key) return { [key]: theme(key, rest) };
}

function tintShade([weight, ...color], { theme }, id) {
  const [kind, action] = id.split("-");
  const mixColor = { tint: "#fff", shade: "#000" }[action];
  const [themeName, prop] = {
    bg: ["backgroundColor", "background-color"],
    text: ["textColor", "color"],
  }[kind];
  return {
    [`--tw-${kind}-opacity`]: "1",
    [prop]: rgba(
      mix(mixColor, theme(themeName, color), theme("opacity", weight)),
      `var(--tw-${kind}-opacity)`
    ),
  };
}
