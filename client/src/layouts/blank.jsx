import { tw } from "twind";

function Blank({ header: Header, content: Content, footer: Footer, ...props }) {
  return (
    <div className={tw`min-h-screen flex flex-col`}>
      {Header && <Header />}
      <div className={tw`flex-grow flex flex-col items-center justify-around`}>
        {Content && <Content {...props} />}
        <div />
      </div>
      {Footer && <Footer />}
    </div>
  );
}

export default Blank;
