import { lazy } from "react";

const routes = [
  {
    path: "/login",
    content: lazy(() => import("pages/login.jsx")),
  },
  {
    path: "/logout",
    content: lazy(() => import("pages/logout.jsx")),
  },
];

export default routes;
