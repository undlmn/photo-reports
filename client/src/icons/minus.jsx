import Icon from "./icon.jsx";

function Minus(props) {
  return (
    <Icon {...props}>
      <line x1="5" y1="12" x2="19" y2="12" />
    </Icon>
  );
}

export default Minus;
