import Icon from "./icon.jsx";

function ArrowUpRight(props) {
  return (
    <Icon {...props}>
      <line x1="7" y1="17" x2="17" y2="7" />
      <polyline points="7 7 17 7 17 17" />
    </Icon>
  );
}

export default ArrowUpRight;
