import Icon from "./icon.jsx";

function Pause(props) {
  return (
    <Icon {...props}>
      <rect x="6" y="4" width="4" height="16" />
      <rect x="14" y="4" width="4" height="16" />
    </Icon>
  );
}

export default Pause;
