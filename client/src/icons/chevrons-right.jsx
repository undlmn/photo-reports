import Icon from "./icon.jsx";

function ChevronsRight(props) {
  return (
    <Icon {...props}>
      <polyline points="13 17 18 12 13 7" />
      <polyline points="6 17 11 12 6 7" />
    </Icon>
  );
}

export default ChevronsRight;
