import Icon from "./icon.jsx";

function SkipForward(props) {
  return (
    <Icon {...props}>
      <polygon points="5 4 15 12 5 20 5 4" />
      <line x1="19" y1="5" x2="19" y2="19" />
    </Icon>
  );
}

export default SkipForward;
