import Icon from "./icon.jsx";

function Navigation(props) {
  return (
    <Icon {...props}>
      <polygon points="3 11 22 2 13 21 11 13 3 11" />
    </Icon>
  );
}

export default Navigation;
