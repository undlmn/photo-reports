import Icon from "./icon.jsx";

function Smartphone(props) {
  return (
    <Icon {...props}>
      <rect x="5" y="2" width="14" height="20" rx="2" ry="2" />
      <line x1="12" y1="18" x2="12.01" y2="18" />
    </Icon>
  );
}

export default Smartphone;
