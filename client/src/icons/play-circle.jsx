import Icon from "./icon.jsx";

function PlayCircle(props) {
  return (
    <Icon {...props}>
      <circle cx="12" cy="12" r="10" />
      <polygon points="10 8 16 12 10 16 10 8" />
    </Icon>
  );
}

export default PlayCircle;
