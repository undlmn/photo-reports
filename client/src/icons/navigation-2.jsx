import Icon from "./icon.jsx";

function Navigation2(props) {
  return (
    <Icon {...props}>
      <polygon points="12 2 19 21 12 17 5 21 12 2" />
    </Icon>
  );
}

export default Navigation2;
