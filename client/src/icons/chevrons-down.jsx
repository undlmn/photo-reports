import Icon from "./icon.jsx";

function ChevronsDown(props) {
  return (
    <Icon {...props}>
      <polyline points="7 13 12 18 17 13" />
      <polyline points="7 6 12 11 17 6" />
    </Icon>
  );
}

export default ChevronsDown;
