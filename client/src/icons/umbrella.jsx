import Icon from "./icon.jsx";

function Umbrella(props) {
  return (
    <Icon {...props}>
      <path d="M23 12a11.05 11.05 0 0 0-22 0zm-5 7a3 3 0 0 1-6 0v-7" />
    </Icon>
  );
}

export default Umbrella;
