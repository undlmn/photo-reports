import Icon from "./icon.jsx";

function Square(props) {
  return (
    <Icon {...props}>
      <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
    </Icon>
  );
}

export default Square;
