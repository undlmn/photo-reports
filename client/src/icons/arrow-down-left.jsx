import Icon from "./icon.jsx";

function ArrowDownLeft(props) {
  return (
    <Icon {...props}>
      <line x1="17" y1="7" x2="7" y2="17" />
      <polyline points="17 17 7 17 7 7" />
    </Icon>
  );
}

export default ArrowDownLeft;
