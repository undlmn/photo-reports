import Icon from "./icon.jsx";

function Disc(props) {
  return (
    <Icon {...props}>
      <circle cx="12" cy="12" r="10" />
      <circle cx="12" cy="12" r="3" />
    </Icon>
  );
}

export default Disc;
