import PropTypes from "prop-types";
import { tw } from "twind";
import Icon from "./icon.jsx";

Spinner.propTypes = {
  gap: PropTypes.number, // %
};

function Spinner({ gap = 70, ...props }) {
  const dash = (2 * Math.PI * 10 * (100 - gap)) / 100;
  props.tw = [props.tw, "animate-spin"].join(" ").trim();

  return (
    <Icon role="presentation" {...props}>
      <circle cx="12" cy="12" r="10" className={tw`opacity-25`} />
      <circle
        cx="12"
        cy="12"
        r="10"
        strokeDasharray={`${dash},100`}
        strokeLinecap="butt"
      />
    </Icon>
  );
}

export default Spinner;
