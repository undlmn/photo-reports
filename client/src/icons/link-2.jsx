import Icon from "./icon.jsx";

function Link2(props) {
  return (
    <Icon {...props}>
      <path d="M15 7h3a5 5 0 0 1 5 5 5 5 0 0 1-5 5h-3m-6 0H6a5 5 0 0 1-5-5 5 5 0 0 1 5-5h3" />
      <line x1="8" y1="12" x2="16" y2="12" />
    </Icon>
  );
}

export default Link2;
