import Icon from "./icon.jsx";

function Circle(props) {
  return (
    <Icon {...props}>
      <circle cx="12" cy="12" r="10" />
    </Icon>
  );
}

export default Circle;
