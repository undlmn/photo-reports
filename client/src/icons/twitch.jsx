import Icon from "./icon.jsx";

function Twitch(props) {
  return (
    <Icon {...props}>
      <path d="M21 2H3v16h5v4l4-4h5l4-4V2zM11 11V7M16 11V7" />
    </Icon>
  );
}

export default Twitch;
