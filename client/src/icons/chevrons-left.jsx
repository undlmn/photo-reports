import Icon from "./icon.jsx";

function ChevronsLeft(props) {
  return (
    <Icon {...props}>
      <polyline points="11 17 6 12 11 7" />
      <polyline points="18 17 13 12 18 7" />
    </Icon>
  );
}

export default ChevronsLeft;
