import Icon from "./icon.jsx";

function ChevronsUp(props) {
  return (
    <Icon {...props}>
      <polyline points="17 11 12 6 7 11" />
      <polyline points="17 18 12 13 7 18" />
    </Icon>
  );
}

export default ChevronsUp;
