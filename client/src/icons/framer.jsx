import Icon from "./icon.jsx";

function Framer(props) {
  return (
    <Icon {...props}>
      <path d="M5 16V9h14V2H5l14 14h-7m-7 0l7 7v-7m-7 0h7" />
    </Icon>
  );
}

export default Framer;
