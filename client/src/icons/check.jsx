import Icon from "./icon.jsx";

function Check(props) {
  return (
    <Icon {...props}>
      <polyline points="20 6 9 17 4 12" />
    </Icon>
  );
}

export default Check;
