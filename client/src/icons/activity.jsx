import Icon from "./icon.jsx";

function Activity(props) {
  return (
    <Icon {...props}>
      <polyline points="22 12 18 12 15 21 9 3 6 12 2 12" />
    </Icon>
  );
}

export default Activity;
