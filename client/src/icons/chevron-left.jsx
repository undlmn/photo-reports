import Icon from "./icon.jsx";

function ChevronLeft(props) {
  return (
    <Icon {...props}>
      <polyline points="15 18 9 12 15 6" />
    </Icon>
  );
}

export default ChevronLeft;
