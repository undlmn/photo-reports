import Icon from "./icon.jsx";

function CornerLeftDown(props) {
  return (
    <Icon {...props}>
      <polyline points="14 15 9 20 4 15" />
      <path d="M20 4h-7a4 4 0 0 0-4 4v12" />
    </Icon>
  );
}

export default CornerLeftDown;
