import Icon from "./icon.jsx";

function MinusCircle(props) {
  return (
    <Icon {...props}>
      <circle cx="12" cy="12" r="10" />
      <line x1="8" y1="12" x2="16" y2="12" />
    </Icon>
  );
}

export default MinusCircle;
