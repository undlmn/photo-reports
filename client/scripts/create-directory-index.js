// Create directory index.js with re-exports defaults

// Use: node create-directory-index.js <source_directory> [...<exclude_files>]

const Path = require("path");
const { readdirSync, readFileSync, writeFileSync } = require("fs");

const [, scriptPath, sourceDir, ...excludeFiles] = process.argv;
if (sourceDir == null) {
  console.log(
    `Use: node ${Path.relative(
      Path.resolve(),
      scriptPath
    )} <source_directory> [...<exclude_files>]`
  );
  process.exit();
}

let result = "";
readdirSync(sourceDir).map((filename) => {
  // if (filename === "index.js") {
  //   console.log("Error: index.js already exists");
  //   process.exit(1);
  // }
  if (
    (filename.endsWith(".js") || filename.endsWith(".jsx")) &&
    !excludeFiles.includes(filename)
  ) {
    const content = readFileSync(Path.join(sourceDir, filename), "utf8");
    const name = (content.match(/export default (\w+);/) || [])[1];
    if (name != null) {
      result += `export { default as ${name}} from "./${filename}";\n`;
    }
  }
});

const targetFile = Path.join(sourceDir, "index.js");
console.log(`Write: ${targetFile}`);
writeFileSync(targetFile, result);
