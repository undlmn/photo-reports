import { setup as twindSetup } from "twind";
import { options } from "../src/twind.js";

twindSetup(options);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    expanded: true,
  },
};

export const globalTypes = {
  theme: {
    name: "Theme",
    description: "Global theme for components",
    defaultValue: "light",
    toolbar: {
      icon: "circlehollow",
      items: [
        { value: "light", left: "☀️", title: "Light" },
        { value: "dark", left: "🌙", title: "Dark" },
      ],
      showName: true,
    },
  },
};

const themeInterceptor = (Story, context) => {
  document.documentElement.classList[
    context.globals.theme === "dark" ? "add" : "remove"
  ]("dark");
  return <Story />;
};

export const decorators = [themeInterceptor];
